/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.service;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test case for checking liquibase changeset.
 * @author karan
 */
public class TestLiquibaseChangeSet {

    public static final Logger LOG = LoggerFactory.getLogger(TestLiquibaseChangeSet.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.debug("Executing setUpBeforeClass");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.debug("Executing tearDownAfterClass");
    }

    @Test
    public void testChangeSet() throws Exception {
        LiquibaseOperations.dropDatabase();
        LiquibaseOperations.createDatabase();
        int errorCode = LiquibaseOperations.createSchemaThroughChangeSet();
        assertEquals(0, errorCode);

        LiquibaseOperations.cleanTables();
        LOG.debug("Done.");
    }
}
